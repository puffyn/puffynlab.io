#!/bin/bash

# https://discourse.gohugo.io/t/gitlab-ci-hugo-module-error/28956/6

# https://gohugo.io/commands/hugo_mod_clean
hugo mod clean

# https://gohugo.io/commands/hugo_mod_get
# hugo mod get -u
hugo mod get --environment="production" -u ./...

# https://gohugo.io/commands/hugo_mod_tidy
hugo mod tidy  --environment="production"

# https://gohugo.io/commands/hugo_mod_vendor/
hugo mod vendor --environment="production"


git add _vendor
git add go.*

# include UNIX timestamp in commit message
git commit -m "chore: production module-vendoring pre-push hook for GitLab CI $(date +%s)"

#-----------------------------------------------------------------------
# TEMP

# I prefer writing tmp files to external HDD instead of laptop SSD,
# so I've set the following in my shell-agnostic env vars imports file
# for zsh/bash:
# export HUGO_CACHEDIR="/run/media/ddg/wslv/tmp_ext_wslv/hugo_cache"

# my open issue:
# https://discourse.gohugo.io/t/are-environment-specific-modules-possible-or-am-i-seeking-the-wrong-solution/31985
